# Install geckodriver

```
$ wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-macos.tar.gz

$ gunzip geckodriver-v0.23.0-macos.tar.gz

$ tar -xf geckodriver-v0.23.0-macos.tar

$ rm geckodriver-v0.23.0-macos.tar
```

# Run Integration Tests:

`$ mvn integration-test`

or
```
$ gradle seleniumTests
$ gradle cucumberTests
```

# Notes
If geckodriver already exists in the system, just change the classpath to it:

`$ export PATH=$PATH:GECKODRIVER_PATH`