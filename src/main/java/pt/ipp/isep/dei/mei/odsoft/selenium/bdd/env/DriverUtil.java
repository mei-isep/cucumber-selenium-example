package pt.ipp.isep.dei.mei.odsoft.selenium.bdd.env;

import org.openqa.selenium.WebDriver;

/**
 * Class used to override the way of obtaining geckodriver-v0.23.0-macos.
 * This way, it does not require changes to the class path.
 */
public class DriverUtil extends env.DriverUtil {

    public static WebDriver getDefaultDriver() {
        //Set the geckodriver-v0.23.0-macos location
        System.setProperty("webdriver.gecko.driver", "libs/geckodriver-v0.23.0-macos");
        return env.DriverUtil.getDefaultDriver();

    }

    public static void closeDriver() {
        env.DriverUtil.closeDriver();
    }

}
