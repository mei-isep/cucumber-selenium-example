package pt.ipp.isep.dei.mei.odsoft.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

// 5th iteration
// 10th iteration

public class CheckoutSteps {

    // 4th iteration
    // int bananaPrice = 0;

    // 10th iteration
    HashMap<String, Integer> fruitPrice = new HashMap<String, Integer>();

    // 6th iteration
    Checkout checkout = null;

    @Given("^the price of a \"(.*?)\" is (\\d+)c$")
    public void thePriceOfAIsC(String arg1, int arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        // First iteration:
        // throw new PendingException();

        // Second iteration:
        // int bananaPrice = arg2;

        // 3th iteration
        // bananaPrice = arg2;

        // 10th iteration
        fruitPrice.put(arg1, arg2);
    }

    @When("^I checkout (\\d+) \"(.*?)\"$")
    public void iCheckout(int arg1, String arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        // First iteration:
        //throw new PendingException();

        // 3th iteration
        //Checkout checkout = new Checkout();
        //checkout.add(arg1, bananaPrice);

        // 4th iteration
        // Checkout checkout = new Checkout();
        // checkout.add(arg1, bananaPrice);

        // 6th iteration
        //checkout = new Checkout();
        //checkout.add(arg1, bananaPrice);

        // 9th iteration
        // if (checkout == null) {
        //   checkout = new Checkout();
        // }
        // checkout.add(arg1, bananaPrice);

        // 10th iteration
        if (checkout == null) {
            checkout = new Checkout();
        }
        checkout.add(arg1, fruitPrice.get(arg2));
    }

    @Then("^the total price should be (\\d+)c$")
    public void theTotalPriceShouldBeC(int arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        // First iteration
        // throw new PendingException();

        // 5th iteration
        assertEquals(arg1, checkout.total());
    }
}
