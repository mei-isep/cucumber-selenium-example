package pt.ipp.isep.dei.mei.odsoft.selenium.simple;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SeleniumStandaloneTest {

    WebDriver driver;

    /**
     * Set the geckodriver-v0.23.0-macos location
     */
    @BeforeAll
    public void setup() {
        //Set the geckodriver-v0.23.0-macos location
        System.setProperty("webdriver.gecko.driver",
                "libs/geckodriver-v0.23.0-macos");

        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(true);

        driver = new FirefoxDriver(options);
    }

    /**
     * Close the browser
     */
    @AfterAll
    public void tearDown() {
        driver.close();
    }

    @Test
    public void navigateToGoogle() {
        driver.navigate().to("https://google.com");
    }

    @Test
    public void navigateToBing() {
        driver.navigate().to("https://bing.com");
    }
}
