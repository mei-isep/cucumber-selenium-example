package pt.ipp.isep.dei.mei.odsoft.selenium.bdd;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import pt.ipp.isep.dei.mei.odsoft.selenium.bdd.env.DriverUtil;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/selenium", "json:target/selenium.json"})
public class LoginTest {

    @BeforeAll
    public void setup() {
        System.setProperty("webdriver.gecko.driver", "libs/geckodriver-v0.23.0-macos");
    }

    @AfterAll
    public void tearDown() {
        DriverUtil.closeDriver();
    }
}
